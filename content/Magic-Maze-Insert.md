---
author: "me"
title: "Custom Insert für Magic Maze"
date: "2021-12-17"
description: ""
tags: ["Basteln"]
ShowToc: false
draft: true
---

An dieser Stelle möchte ich von einem Projekt berichten, welches ich letzten Sommer gemacht habe.  
Nachdem ich die Erweiterung Maximum Security für Magic Maze gekauft hatte, passten die Sachen nicht mehr wirklich gut in die Box.
