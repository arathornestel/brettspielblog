---
title: "Datenschutzerklärung"
comments: "false"
hidemeta: "true"
ShowPostNavLinks: "false"
ShowToc: true
url: "/datenschutz/"
_build:
  list: never
---

### Informationen über mich als Verantwortlicher

Ich bin eine Privatperson die gerne Brett- und Kartenspiele spielt und zudem auch gerne darüber spricht. Um nicht meiner Umgebung auf die Nerven zu gehen schreibe ich hier auf diesem Blog an hoffentlich Interessierte Menschen. Es wird weder Werbung geschaltet noch bin ich in irgendeiner Form von Verlagen oder Autoren beeinflusst.

Verantwortlicher Anbieter dieses Internetauftritts und der hier angebotenen Services im rechtlichen Sinne sowie Verantwortlicher im Sinne des Datenschutzes ist:  
Sebastian Pisot  
E-Mail: [brettspielrat{at}posteo.de](mailto:brettspielrat@posteo.de)  
Impressum: https://brettspielrat.de/impressum

### Rechte der Nutzerinnen und Betroffenen

Du hast einige Rechte die du allein schon von Rechtswegen bei uns geltend machen kannst. Diese sind leider ein bisschen trocken zu lesen, aber eine wichtige Errungenschaft im Kampf um die Souveränität deiner Daten und durch die DSGVO fest verankert.
Du hast folgende Rechte, die sich insbesondere aus Art. 15 bis 21 DSGVO ergeben:

* **Widerspruchsrecht:** Sie haben das Recht, aus Gründen, die sich aus Ihrer besonderen Situation ergeben, jederzeit gegen die Verarbeitung der Sie betreffenden personenbezogenen Daten, die aufgrund von Art. 6 Abs. 1 lit. e oder f DSGVO erfolgt, Widerspruch einzulegen; dies gilt auch für ein auf diese Bestimmungen gestütztes Profiling. Werden die Sie betreffenden personenbezogenen Daten verarbeitet, um Direktwerbung zu betreiben, haben Sie das Recht, jederzeit Widerspruch gegen die Verarbeitung der Sie betreffenden personenbezogenen Daten zum Zwecke derartiger Werbung einzulegen; dies gilt auch für das Profiling, soweit es mit solcher Direktwerbung in Verbindung steht.
* **Widerrufsrecht bei Einwilligungen:** Sie haben das Recht, erteilte Einwilligungen jederzeit zu widerrufen.
* **Auskunftsrecht:** Sie haben das Recht, eine Bestätigung darüber zu verlangen, ob betreffende Daten verarbeitet werden und auf Auskunft über diese Daten sowie auf weitere Informationen und Kopie der Daten entsprechend den gesetzlichen Vorgaben.
* **Recht auf Berichtigung:** Sie haben entsprechend den gesetzlichen Vorgaben das Recht, die Vervollständigung der Sie betreffenden Daten oder die Berichtigung der Sie betreffenden unrichtigen Daten zu verlangen.
* **Recht auf Löschung und Einschränkung der Verarbeitung:** Sie haben nach Maßgabe der gesetzlichen Vorgaben das Recht, zu verlangen, dass Sie betreffende Daten unverzüglich gelöscht werden, bzw. alternativ nach Maßgabe der gesetzlichen Vorgaben eine Einschränkung der Verarbeitung der Daten zu verlangen.
* **Recht auf Datenübertragbarkeit:** Sie haben das Recht, Sie betreffende Daten, die Sie uns bereitgestellt haben, nach Maßgabe der gesetzlichen Vorgaben in einem strukturierten, gängigen und maschinenlesbaren Format zu erhalten oder deren Übermittlung an einen anderen Verantwortlichen zu fordern.
* **Beschwerde bei Aufsichtsbehörde:** Sie haben unbeschadet eines anderweitigen verwaltungsrechtlichen oder gerichtlichen Rechtsbehelfs das Recht auf Beschwerde bei einer Aufsichtsbehörde, insbesondere in dem Mitgliedstaat ihres gewöhnlichen Aufenthaltsorts, ihres Arbeitsplatzes oder des Orts des mutmaßlichen Verstoßes, wenn Sie der Ansicht sind, dass die Verarbeitung der Sie betreffenden personenbezogenen Daten gegen die Vorgaben der DSGVO verstößt.

### Bereitstellung des Onlineangebotes

Um unser Onlineangebot sicher und effizient bereitstellen zu können, nehme ich die Leistungen von Webhosting-Anbietern in Anspruch, von deren Servern (bzw. von ihnen verwalteten Servern) das Onlineangebot abgerufen werden kann.

#### Serverstandorte und Serveranbieter

Die Seite liegt auf einem Webspace beim Provider [uberspace](https://uberspace.de). Deren Serverstandorte sind in Deutschland, und unterliegen somit den europäischen sowie deutschen (Datenschutz)Gesetzen. Die Domain wurde bei [Biohost](https://biohost.de) registriert.

#### Logdaten

Dort wird ein Access-log geführt das mir bei Problemen hilft rauszufinden was nicht funktioniert. Das könnt ihr nicht untersagen oder verhindern, falls ihr also damit ein Problem habt könnt ihr meinen Blog leider nicht besuchen.

folgende Daten können dabei erhoben werden:
* Typ und Version deines Internetbrowsers
* das Betriebssystem
* die Webseite, von der aus du auf meinen Blog gewechselt hast (Referrer-URL)
* eine Liste der Webseite(n), die du bei mir besuchst (genaue Adresse)
* Datum und Uhrzeit des jeweiligen Zugriffs
* die IP-Adresse (gekürzt s.u.) des Internetanschlusses, von dem aus die Nutzung unseres Angebotes erfolgt

>Um die Privatsphäre der Nutzer zu schützen, protokollieren wir nur die ersten 16 Bits einer IPv4-Adresse bzw. die ersten 32 Bits >einer IPv6-Adresse und löschen den Rest. So werden die IPv4-Adresse von uberspace.de, 82.98.87.93, und die IPv6-Adresse >2a02:2e0:3fc:52:0:62:5768:38 als 82.98.0.0 und 2a02:2e0:: in den aktuellen Logdateien protokolliert.
>Die Protokolldateien werden täglich rotiert und nach 7 Tagen gelöscht.

*übersetzter Auszug aus https://manual.uberspace.de/web-logs/

#### Datenübertragung und Datensicherheit

Um eure via unserem Online-Angebot übermittelten Daten zu schützen, nutze ich eine SSL-Verschlüsselung. Ihr erkennt derart verschlüsselte Verbindungen an dem Präfix https:// in der Adresszeile eures Browsers. Das dafür benötigte Zertifikat stammt von [let's encrypt](https://letsencrypt.org/). 

### Tracking

Um herauszufinden welche Beiträge interessant sind und wie häufig Besucher auf der Seite sind nutze ich Tracking. Dabei greife ich auf eine von mir betriebene Installation von[Open Web Analytics](https://www.openwebanalytics.com/) zurück. Die Daten liegen somit ebenfalls auf den Servern von Uberspace. In den Konfigurationseinstellungen wurde eingestellt das die IP-Adressen um das letzte Achtbitzeichen gekürzt werden.


### Änderung und Aktualisierung der Datenschutzerklärung

Wir bitten Sie, sich regelmäßig über den Inhalt unserer Datenschutzerklärung zu informieren. Wir passen die Datenschutzerklärung an, sobald die Änderungen der von uns durchgeführten Datenverarbeitungen dies erforderlich machen. Wir informieren Sie, sobald durch die Änderungen eine Mitwirkungshandlung Ihrerseits (z.B. Einwilligung) oder eine sonstige individuelle Benachrichtigung erforderlich wird.
Sofern wir in dieser Datenschutzerklärung Adressen und Kontaktinformationen von Unternehmen und Organisationen angeben, bitten wir zu beachten, dass die Adressen sich über die Zeit ändern können und bitten die Angaben vor Kontaktaufnahme zu prüfen.

*[Erstellt unter zuhilfenahme des kostenlosen Datenschutz-Generator.de von Dr. Thomas Schwenke](https://datenschutz-generator.de/). Passagen sind teilweise ergänzt oder gänzlich ersetzt.*
