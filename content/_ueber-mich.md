---
title: "Über mich"
comments: "false"
hidemeta: "true"
ShowPostNavLinks: "false"
url: "/ueber-mich/"
_build:
  list: never
---
## Wer bin ich?
### Die Kurzfassung
Ich bin eine Privatperson die gerne Brett- und Kartenspiele spielt und zudem auch gerne darüber spricht. Um nicht meiner Umgebung auf die Nerven zu gehen schreibe ich hier auf diesem Blog an hoffentlich Interessierte Menschen. Es wird weder Werbung geschaltet noch bin ich in irgendeiner Form von Verlagen oder Autoren beeinflusst.

### Die Langfassung
Für alle die es genauer wissen wollen ;) Ich bin Baujahr 1995, studiere Landschaftsarchitektur in Freising und bin begeistert von Brettspielen. Und das im Prinzip eigentlich schon immer, nur in unterschiedlicher Itensität.  
Nach den Kinderspielen sind die ersten großen Spiele an die ich mich erinnern kann Auf Achse, Monopoly, Siedler von Catan, Schach und Herr der Ringe - Risiko.   Über die Pfadfinder vom BDP, die zweimal im Jahr große Brettspielwochenenden veranstalten kam ich dann mit immer mehr Brettspielen in Berührung. Spätestens 2016 wo ich mit Leuten von besagtem BDP zur SPIEL in Essen gefahren bin, war ich vollständig im Hobby angekommen. Dort machte ich dann erste Bekanntschaft mit BloodRage, War of the Ring und mit Blood Bowl auch dem ersten Tabletop-System.  
In den folgenden Jahren hatte ich mit zwei Freunden alle paar Monate eine Brettspielrunde bei der wir vorallem War of the Ring und Sid Meyers Civilization gezockt haben.  
In Freising war ich auch oft im lokalen Spieleladen wo ich dann Magic entdeckte und sofort begeistert war. Nach zwei Ligen viel aber dann deutlich auf wie viel Geld man da immer wieder auf den Tisch legen muss und ich verabschiedetete mich davon. Auch wenn mich das Spielprinzip, insbesondere der Draft nach wie vor überzeugt. Ungefähr zur selben Zeit entdeckte ich dort dann auch X-Wing das Miniaturenspiel, das ich eine zeitlang häufig mit einer bekannten von Foodsharing gezockt habe. Außerdem habe ich darüber den Youtube-Kanal TWS und dadurch meine Begeisterung für den Modellbau entdeckt. Seither bin ich auch in der Welt der Tabletops unterwegs, durch einen Kumpel dann auch noch 40k allerdings bin ich nie besonders viel zum spielen gekommen.  
Dann kam die Pandemie...  
Die hat bei mir eigentlich drei Dinge verändert. Zum einen bin ich zu dem Zeitpunkt mit meiner Ex-Freundin zusammengezogen, was bei den Spielen natürlich zu zweit gut spielbare Spiele in den Fokus gerückt hat. Durch die Reduzierung auf Online-Treffen habe ich dann mit Freunden verschiedene Rollenspiel-Systeme (Shadowrun, Fate - Harry Potter, Star Wars - Zeitalter der Rebellion) ausprobiert. Die Inspiration dafür kam vorallem über die Kanäle von Mhaire Stritter: Orkenspalter TV und Mitch. Außerdem, was eigentlich ziemlich paradox ist weil ich ja gar nicht groß spielen konnte, habe ich meine Brettspielsammlung weiter ausgebaut.  

## Was will ich?
So hundertprozentig muss ich ganz ehrlich sagen weiß ich das nicht. Aber ich möchte einfach Leute von Brettspielen begeistern und insbesondere auch versuchen dabei Spiele, Themen etc. zu betonen die nicht tausend andere schon viel besser bearbeiten. Dabei ist mir insbesondere der Umweltschutz, Kapitalismuskritik und Antifaschismus wichtig. Außerdem ist der Blog für mich auch eine Möglichkeit an einer Homepage mit echten Inhalten zu basteln wodurch ich meinem Interesse für IT, insbesondere Opensource-Software, nachgehen kann.

## Was will ich nicht?
Spiele nach einem Bewertungssystem vergleichen und einordnen. Ich glaube sowas macht nur wenig Sinn, letzendlich kommt es darauf an ob ein Spiel zu dir und deinen Mitspielerinnen passt. Dabei kann es natürlich sein das es objektiv betrachtet ein schlechtes Spiel ist aber solange ihr Spaß habt ist das ja völlig egal. Ich werde außerdem auch nicht großartig Zeit mit spielen verlieren die mir nicht gefallen; es gibt schließlich so viele Spiele, das ich mich lieber auf die konzentriere die ich auch gut finde, die anderen überlasse ich dann lieber Menschen die sie gut finden.