---
title: "Impressum"
comments: "false"
hidemeta: "true"
ShowPostNavLinks: "false"
url: "/impressum/"
_build:
  list: never
---

Ich bin eine Privatperson die gerne Brett- und Kartenspiele spielt und zudem auch gerne darüber spricht. Um nicht meiner Umgebung auf die Nerven zu gehen schreibe ich hier auf diesem Blog an hoffentlich Interessierte Menschen. Es wird weder Werbung geschaltet noch bin ich in irgendeiner Form von Verlagen oder Autoren beeinflusst.

### Angaben gemäß § 5 TMG:

#### Betreiber der Website
Sebastian Pisot  
c/o [autorenglück.de](https://autorenglück.de)  
Franz-Mehring-Str. 15  
01237 Dresden

#### Kontakt

E-Mail: [brettspielrat{at}posteo.de](mailto:brettspielrat@posteo.de)  
Fediverse: <a rel="me" href="https://rollenspiel.social/@brettspielrat">Mastodon</a>


### Haftungsausschluss:
#### Haftung für Inhalte

Gemäß § 7 Abs.1 TMG trage ich nach den allgemeinen Gesetzen die Verantwortung für eigene Inhalte auf diesen Seiten. Allerdings bin ich als Dienstanbieter nach §§ 8 bis 10 TMG nicht verpflichtet, übermittelte oder gespeicherte fremde Informationen zu überwachen oder nach Umständen zu forschen, die auf eine rechtswidrige Tätigkeit hindeuten. Davon ausgeschlossen sind die Verpflichtungen zur Entfernung bzw. Sperrung der Nutzung von Informationen nach den allgemeinen Gesetzen. Diesbezüglich ist eine Haftung allerdings erst ab dem Zeitpunkt der Kenntnis einer konkreten Rechtsverletzung möglich. Sollte ich von einer entsprechenden Rechtsverletzung erfahren, werde ich die betreffenden Inhalte umgehend entfernen.

#### Haftung für Links

Trotz sorgfältiger Kontrolle übernehme ich keine Haftung für externe Links, auf deren Inhalt ich keinen Einfluss haben. Für den Inhalt der verlinkten Seiten sind ausschließlich deren Betreiber verantwortlich. Zum Zeitpunkt der Verlinkung habe ich die externen Links auf mögliche Rechtsverstöße überprüft und konnte keine rechtswidrigen Inhalte feststellen. Ohne konkrete Anhaltspunkte ist eine durchgehende Kontrolle der Inhalte jedoch nicht zumutbar. Sollte ich diesbezüglich von einer Rechtsverletzung erfahren, werde ich die betreffenden Links umgehend entfernen.

#### Urheberrecht

Die Inhalte und Werke auf diesen Seiten, die durch die Seitenbetreiber erstellt wurden, unterliegen der CreativeCommons-Lizens [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode.de). Für die Vervielfältigung, Bearbeitung, Verbreitung ist die Angabe von mir als Verfasser notwendig. Dabei darf das entstanndene Material nur unter der gleichen Lizens und ausschließlich nicht-kommerziell verbreitet werden. Inhalte Dritter werden als solche gekennzeichnet und unterliegen den Urheberrechten Dritter. Sollte ich diesbezüglich von einer Rechtsverletzung erfahren, werde ich die betreffenden Inhalte umgehend entfernen.
