---
author: "me"
title: "Meine Jogginghosenspiele"
date: 2022-05-03T20:47:46+02:00
description: ""
tags: ["topliste"]
ShowToc: false
---

Ich habe die Tage ein [Video](https://redirect.invidious.io/watch?v=h_gSO4E2okE) von Betterboardgames mit Maren Hoffmann geschaut. Dort prägt sie den Begriff Jogginghosenspiel.  
Die Kurzfassung davon ist:
"Ein Spiel das man so gut kennt, das es Gemütlichkeit beim spielen hervorbringt. z.b. dadurch das man die Regeln in und auswendig kennt."

Ich fand den Ansatz sehr spannend, daher habe ich mir mal Gedanken gemacht welche Spiele das bei mir wären:

1. [Fantastische Reiche](/fantastische-reiche) (mit meiner Freundin)

2. [Dominion](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=22017) (mit vielen verschiedenen Leuten, vorallem auch in der [Online-Version](http://dominion.game))

3. [Brawl](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=12296) (mit meiner Freundin)

5. [Magic Maze](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=31690) (zusammen mit meiner Schwester)

Eine Zeitlang war tatsächlich sogar [War of the Ring](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=25262) ein Jogginghosenspiel von mir und zwei Freunden, leider hab ich es jetzt schon viel zu lange nicht mehr gespielt als das ich es noch so gut können würde. Weitere Anwärter die ich aber bis jetzt noch nicht oft genug gespielt habe wären:
* [Galaxy Trucker](https://boardgamegeek.com/boardgame/336794/galaxy-trucker)
* [Das Streben nach Glück](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=33571)
* [7 Wonders Duel](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=28657)