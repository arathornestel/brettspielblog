---
author: "me"
title: "Gespielt Dezember"
date: 2022-01-03T19:14:21+01:00
description: "Nach Spielerinnenanzahl sortierte Liste der Spiele die ich im Dezember gespielt habe. Teilweise mit ergänzenden Infos."
schlagworte: ["gespielt"]
ShowToc: false
cover:
    image: "/img/icecool.jpg"
   # can also paste direct link from external site
   # ex. https://i.ibb.co/K0HVPBd/paper-mod-profilemode.png
    alt: "<Coverimage Spiel IceCool>"
    caption: "<text>"
    hidden: true
    relative: false # To use relative path for cover image, used in hugo Page-bundles
---


### 2 Spielerinnen
* #### Dungeon Twister (5mal)
Gefällt mir super. Gefunden im [Klassiker-Video von Betterboardgames](https://redirect.invidious.io/watch?v=YWhCoNzm9xE). (Der Link führt zur Instanzauswahl von Invidious.)
![Spieleindruck von DungeonTwister](/img/dungeon-twister.jpg)

### 2-4 Spielerinnen
* #### [Bios:Genesis](https://boardgamegeek.com/boardgame/98918/bios-genesis) (1mal)
* #### [Calico](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=36170) (5mal)
* #### [Canvas](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=36228) (4mal)
* #### [Yokai](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=33851) (2mal)
Hat mir gut gefallen, spannende gemeinsame Hirnverrenkungen.
* #### [Dungeon Roll](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=26214) (1mal)
Kleines Push-your-luck Würfelspiel mit einer gewissen Ähnlichkeit zu Zombidice, wenn auch etwas anspruchsvoller. Gefällt mir im Direktvergleich etwas besser.
* #### IceCool (2mal)
![Spieleindruck von IceCool](/img/icecool.jpg)
* #### Chronicles of crime - 2400 (1mal)
Cooles Tutorial, bin gespannt auf den ersten richtigen Fall.
* #### Dead Man's Draw
Geht aber auch gut zu fünft.

### 2-5 Spielerinnen
* #### My Gold Mine (mindestens 10mal)
Gefällt mir super siehe auch mein Eindruck aus der [Weihnachts-Topliste](https://brettspielrat.de/top3-weihnachten/#1-my-gold-mine).
* #### GOLD (5mal)
Memory mit einem kleinen Twist, so dass darum geht bestimmte Kombinationen aufzudecken statt einfach nur das Gleiche Bildchen.
* #### Tsukuyumi (1mal)
Wir haben die kooperative Version ausprobiert, mein Eindruck nach einer normalen und einer kooperativen Runde ist das mir Das Spiel sehr gut gefällt allerdings ist die kooperative Version deutlich komplizierter und auch irgendwie nervig durch das Steuern der gegnerischen Oni-Fraktion.
Mit der Erweiterung auch zu sechst spielbar.

### 2-6 Spielerinnen
* #### [Fantastische Reiche](https://brettspielrat.de/fantastische-reiche) (15mal)
* #### Lovecraft Letter (1mal)
* #### BlitzDings (1mal)
Witziges Assoziations-Partyspiel.
* #### Schwarzer Peter (1mal)
An Weihnachten auf der Familienfeier trotz besseren Wissen gespielt...
### 2-8 Spielerinnen
* #### Dobble (1mal)








 


