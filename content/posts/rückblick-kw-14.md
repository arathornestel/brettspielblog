---
author: "me"
title: "Rückblick KW 14"
date: 2022-04-12T22:09:45+02:00
description: ""
tags: ["gespielt", "Wochenrückblick"]
ShowToc: false
---

Letztes Wochenende hab ich endlich mal wieder viel gespielt ;) Erst Sonntag Mittag in ner Kneipe zu dritt mindestens 5 Runden [Fantastische Reiche](https://brettspielrat.de/fantastische-reiche), [Kuhhandel](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=11950) und [Die Crew - Mission Tiefsee](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=36131) gespielt.
Abends hatten wir dann zu sechst einen gemütlichen Spieleabend mit [Codenames](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=28670), [Anno Domini](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=10098) und [My Gold Mine](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=36328).
Außerdem ist letzte Woche der Kickstarter von [Human Punishment - The Beginning](https://www.kickstarter.com/projects/13233511/human-punishment-the-beginning) angekommen. Ich bin schon richtig gespannt, auch wenn es wohl noch ein Weilchen Dauern dürfte bis ich das auf den Tisch bekomme.
Und wie vorletzte Woche berichtet habe ich [Blood Rage](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=28648) bekommen, wovon ich mit meiner Freundin zumindest das erste Zeitalter spielen konnte.