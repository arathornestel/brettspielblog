---
author: "me"
title: "Rückblick KW22"
date: 2022-06-07T14:39:37+02:00
description: ""
tags: ["brettspiele","gespielt"]
ShowToc: false
cover:
    image: "/img/cover-rückblick-kw22.jpg"
    # can also paste direct link from external site
    # ex. https://i.ibb.co/K0HVPBd/paper-mod-profilemode.png
    alt: "<Der Stapel der Spiele die im Rückblick Thema sind>"
    caption: "<text>"
    hidden: true
---
Dieses mal gibts den Rückblick von zwei Spieleabenden außerdem habe ich noch zwei digtale Brettspiele ausprobiert.  
Zuerst waren wir Donnerstag Abend bei nem Kumpel. Da ich zuerst da war gabs erstmal ne Runde [7 Wonders Duel](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=28657). Einfach ein super Spiel um gemütlich zu zweit zu zocken ;).  
Danach waren wir dann zu dritt und haben Herr der Fritten und danach noch Scotland Yard gespielt. [Herr der Fritten](https://www.luding.org/cgi-bin/GameName.py?f=00w%5EE4X&gamename=Herr+der+Fritten) ist ein kleines Kartenspiel bei dem wir als Zombies in einem Resteraunt kellnern. Auch wenn der Mechanismus nichts besonderes ist mach das Spiel durch Illustrationen und Namen auf den Resterauntmenus richtig Laune. Zum Klassiker [Scotland Yard](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=26924) brauch ich glaub nicht viel sagen außer das wir festgestellt haben das man schon besser mindestens zu viert ist, was mir nicht mehr so bewusst war.  

Am Sonntag abend war ich dann bei einem Spieltreff in München. Hat mir sehr gut gefallen, werd ich vermutlich jetzt öfter hingehen. Gespielt haben wir als kleinen Einstieg [Verflucht!](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=32186), dann eine Runde [Just One](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=32728), zwischendurch noch eine Runde [Splendor](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=26560) und zum Schluss Dungeon Twister.  

Außerdem konnte ich dank der digitalen Umsetzung noch [Star Realms](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=27731) und [Terra Mystica]() ausprobieren.