---
author: "me"
title: "Rückblick KW21"
date: 2022-06-01T13:19:27+02:00
description: ""
tags: ["rückblick","gespielt","Brettspiele"]
ShowToc: false
---
Letztes Wochenende war ich zum ersten Mal auf den Karlsfelder Spieletagen (Vorort von München). War richtig gemütlich dort und es hat mich sehr gefreut mal wieder die Atmosphäre von vielen Brettspielerinnen in einem Raum genießen zu können. Organisiert wurde das ganze vom [Kornelius-Spieletreff Karsfeld](http://spieletreff-karlsfeld.de/).  

Gespielt habe ich dort am Samstag [Just One](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=32728) ein kooperatives Wort-Assoziations-Spiel, [Wannabe Football](https://luding.org/cgi-bin/GameName.py?f=00w%5EE4X&gamename=Wannabe+football) ein schnelles Kartenspiel bei dem wir gegeneinander Fussball spielen und [Robo Chaos](https://boardgamegeek.com/boardgame/356133/quirky-circuits-penny-gizmos-snow-day) hier versuchen wir einen Staubsaugerroboter mithilfe von Programmierkarten gemeinsam über die Staubpartikel zu steuern. Außerdem gibt es noch einen Skiroboter mit ein wenig anderen Bewegungsregeln. Macht richtig Laune und ist schön chaotisch ;).  

Am Sonntag Abend konnte ich dann noch [Hadara](https://www.luding.org/cgi-bin/GameName.py?f=00w%5EE4X&gamename=Hadara) ausprobieren. Hat mir richtig gut gefallen. Ein schönes, gemütliches und zugängliches Zivilisationsaufbau-Spiel.  
![Playerboard Hadara](/img/hadara-spieleindruck.jpg)
Außerdem war ich letzte Woche noch mit meiner Freundin auf einem Straßenfest wo wir ebenfalls Wannabe Football und außerdem noch [Punto](https://www.luding.org/cgi-bin/GameName.py?f=00w%5EE4X&gamename=Punto) gespielt haben. Letzteres ist ein Kartenlegespiel das im Prinzip wie 4 gewinnt funktioniert, allerdings mit dem Kniff das wir höhere Karten über niedrigere Karten drüber legen können. Außerdem dank der Mini-Blechdose ein perfektes Reise-Spiel.