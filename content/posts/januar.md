---
author: "me"
title: "Gespielt: Januar"
date: 2022-01-08T17:10:38+01:00
description: ""
tags: ["gespielt"]
ShowToc: false
---

### 2 Spielerinnen
* #### Dungeon Twister (5mal)
Gefällt mir super. Gefunden im [Klassiker-Video von Betterboardgames](https://redirect.invidious.io/watch?v=YWhCoNzm9xE). (Der Link führt zur Instanzauswahl von Invidious.)
![Spieleindruck von DungeonTwister](/img/dungeon-twister.jpg)

### 2-4 Spielerinnen
* #### Calico (1mal)
* #### [Feierabend](https://www.luding.org/cgi-bin/GameName.py?f=00w%5EE4X&gamename=Feierabend)
* #### [Robin Hood](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=36124)
* #### [Yokai](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=33851)

### 2-5 Spielerinnen
* #### [My Gold Mine](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=36328) (2 mal)
Gefällt mir super siehe auch mein Eindruck aus der [Weihnachts-Topliste](https://brettspielrat.de/top3-weihnachten/#1-my-gold-mine).
* #### [Gold](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=33877) (1mal)
Memory mit einem Twist. Wir versuchen unsere eigenen Bergarbeiter und dazu passendes Gold aufzudecken.
* #### [Windward](https://boardgamegeek.com/boardgame/282922/windward) (1mal)
Mir gefällt besonders der Bewegungsmechanismus: Bewegungen mit dem Wind sind kostenlos, direkt gegen den Wind kann man sich nicht bewegen, alles andere kostet jeweils ein Bewegungspunkt.


### 2-6 Spielerinnen
* #### [Lovecraft Letter](https://www.luding.org/cgi-bin/GameName.py?f=00w%5EE4X&gamename=lovecraft+letter) (1mal)