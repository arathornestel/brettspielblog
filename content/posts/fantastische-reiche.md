---
author: "me"
title: "Vorstellung: Fantastische Reiche"
date: "2021-12-13"
description: ""
tags: ["Vorstellung"]
ShowToc: false
url: /fantastische-reiche
#draft: true
cover:
    image: "/img/FantastischeReiche.png"
    alt: "Coverimage Spiel Fantastische Reiche"
    hiddenInList: true
info:
    verlag: "Strohmann Games"
    verlaglink: "https://www.strohmann-games.de/" 
    autor: "Bruce Glassco"
    grafik: "Patricia Rodriguez"
    alter: "ab 10 Jahren"
    dauer: "20 Minuten"
    spieler: "2-6"
    kosten: "19,90 €"
---

Meine erste detailliertere Vorstellung, gebt gerne Rückmeldung was ihr euch in zukünftigen Vorstellungen wünschen würdet.

### Meine Erlebnisse mit dem Spiel.  
Zunächst mal der Kauf: ich schau schon seit Monaten immer wieder danach und war jetzt Anfang Dezember dann im Spieleladen Weihnachtsgeschenke kaufen und hab beschlossen auch für mich was mitzunehmen. Hab ich mich dann kurzerhand für Fantastische Reiche entschieden, was im nachhinein eine fantastische Entscheidung war :D  
Obwohl meine Freundin überhaupt kein Fantasy-Typ ist hat sie's ausprobiert und wahr sofort Feuer und Flamme, eigentlich direkt am ersten Tag schon zweimal gespielt und am nächsten morgen auch direkt im Bett nochmal. Als wir dann am nächsten Tag wieder im Spielladen waren hab ich mir dann auch direkt noch die Erweiterung geholt ;)

### Was finde ich so toll:
* Super einfache Regelerklärung: "Ihr zieht 7 bzw. 8 Karten und zieht jede Runde entweder vom Nachziehstapel oder aus der Ablage und legt eine Karte wieder ab. Die Karten stehen miteinander in Beziehung und ihr wollt die Kartenhand mit den meisten Punkten bekommen. Das Spiel endet wenn 10 bzw. 12 Karten in der Auslage liegen." Das reicht im Prinzip schon aus um direkt ne Runde loszulegen, weitere Details können dann währendessen oder vor der zweiten Runde geklärt werden.
* Trotzdem ist es kein seichtes einfaches Spiel, man muss definitiv nachdenken ;)
* Mir gefällt der das grundsätzliche Artwork. Außerdem sind Frauen und Männer gleichermaßen sexualisiert dargestellt.
* Es gibt viele verschiedene Kombinationen, es gibt selten eine gleich Kartenhand.
### Was ich nicht so toll finde:
* Die zusätzlichen Fluchkarten aus der Erweiterung reizen mich irgendwie überhaupt nicht, auch wenn ich's tatsächlich noch nicht mal ausprobiert habe ;(
* Es stellt sich ein wenig die Frage ob die Illus dermaßen sexualisiert dargestellt werden müssen, außerdem sind die Brüste der Frauen ziemlich unrealistisch dargestellt.
* Die Schachtelgröße ist ziemlich seltsam. mit ungesleevten Karten kann man in der Mitte der Box zwei Stapel nebeneinander plazieren die jedoch ziemlich ineinander rutschen, insbesondere da die box zu tief für die Karten ist weil sonst Wertungsblock und Regeln nicht hineinpassen würden. Gesleeved funktioniert es dann überhaupt nicht mehr, daher werd ich das ganze auch umbauen, und demnächst dann davon auch berichten. Für die Erweiterung wäre es eigentlich völlig ausreichend gewesen eine kleine Pappschachtel wie bei einem Skatdeck oder so zu machen da die Karten sowieso in die Grundspielbox reinpassen. Jetzt liegt wieder eine kleine stabile Schachtel rum die ich ungern einfach so wegwerfe aber auch nicht wirklich gebrauchen kann.
