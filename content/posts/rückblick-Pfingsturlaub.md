---
author: "me"
title: "Rückblick Pfingsturlaub"
date: 2022-07-02T09:27:12+02:00
description: ""
tags: ["Rückblick","Brettspiel"]
ShowToc: false
---
So, jetzt schaffe ich es endlich mal wieder was zu schreiben. Der Urlaub hat mich da etwas aus dem Rythmus gebracht. 
Allerdings war der wiederum sehr spielereich ;)
Ich war mit meiner Freundin in Bergen und auf der Hin- und Rückfahrt in Malmö/Copenhagen. Gefahren sind wir mit dem Zug in dem wir Punto gespielt haben.  
![Spieleindruck Punto](/img/punto-spieleindruck.jpeg)
In Copenhagen war ich dann zunächst allein im [Bastard Cafe](https://bastardcafe.dk/), einem richtig coolen Spielecafe mit über 4000 Brettspielen. 
![testtest](/img/bastardcafe.jpg)
Dort hab ich den Solo-Modus von [Ragusa](https://www.luding.org/cgi-bin/GameName.py?lang=DE&frames=0&gamename=ragusa) ausprobiert. Hat mir ganz gut gefallen auch wenn ich kein Solo-Spieler bin. Aber würd ich auf jeden fall gern mal noch mit mehr Menschen ausprobieren wollen.  
![](/img/ragusa-spieleindruck.jpg "Das Spiel gegen Ende einer Solopartie")
In Bergen waren wir zu Gast bei der Schwester meiner Freundin und haben [Fort](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=36598), [Wannabe Football](/review-wannabe-football), [Cabo](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=24681), [Marvel Fluxx](https://boardgamegeek.com/boardgame/272541/marvel-fluxx), [7 Wonders Duel](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=28657), [Herr der Fritten](https://www.luding.org/cgi-bin/GameName.py?f=00w%5EE4X&gamename=Herr+der+Fritten) und [My Gold Mine](https://www.luding.org/cgi-bin/GameName.py?f=00w%5EE4X&gamename=my+gold+mine) gespielt.
Auf der Rückfahrt hab ich dann am Bahnhof in Oslo, nach dem Hinweis meiner Freundin auf einen Tisch mit Brettspielen, [Arctic Race](https://boardgamegeek.com/boardgame/309824/arctic-race) gekauft. Das ist ein dänisches Spiel welches eigentlich nur in Skandinavien und auch nur in den 4 skandinavischen Sprachen verfügbar ist. Aber mit Übersetzer-tools versteht man die Regeln auch ;).
Auf der Rückfahrt haben wir dann noch [Red 7](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=27159) gespielt und erfolgreich ausgetestet ob 7 Wonders Duel im Ice spielbar ist.