---
author: "me"
title: "Review: Wannabe Football"
date: 2022-06-07T14:39:37+02:00
#draft: true
description: ""
tags: ["Review","Brettspiel","2/4spielerinnen"]
ShowToc: false
cover:
    image: "/img/wannabefootball.webp"
    alt: "Coverimage Spiel Fantastische Reiche"
    hiddenInList: true
info:
    verlag: "Game Absorber"
    verlaglink: "https://gameabsorber.dk/" 
    autor: "Erik Atzen & Martin Rasmussen"
    grafik: "Mette Ehlers"
    alter: "ab 8 Jahren"
    dauer: "5 bis 15 Minuten"
    spieler: "2/4"
    kosten: "ca. 20 €"
---
### Was sagt der Verlag?


Wannabe Football ist ein rasantes Fußball-Kartenspiel mit einfachen Regeln für die ganze Familie. Es ist ein Spiel mit Höhepunkten, das das übliche Hin und Her überspringt und direkt zu Torchancen führt. Mit fünf Karten auf der Hand spielst, dribbelst, tackelst und schießt du den Ball, um Tore zu erzielen!  
Wannabe Football enthält 66 Karten ohne Text oder Zahlen auf den Karten.  
Regeln in 5 Sprachen: Englisch, Spanisch, Französisch, Deutsch, Dänisch
### Wie funktionierts?

Wannabe Football ist ein schnelles Kartenspiel für 2 oder 4 Spielerinnen bei dem wir, wie der Name schon sagt gegeneinander Fussball spielen.
Dazu verwenden wir zwei Kartenstapel. Einmal die Spielerkarten mit denen wir uns über das Fussballfeld spielen und die Entscheidungskarten die aufgedeckt werden wann immer eine Entscheidung getroffen wird, wie z.b. ob ein Torschuss erfolgreich war.  

Auf den Spielerkarten ist oben links abgedruckt wo der Ball sich aktuell befindet und wohin er gespielt werden soll. Dabei gibt es 6 mögliche Positionen: der eigene Strafraum in der eigenen Hälfte links und rechts und und dasselbe in der gegerischen Hälfte. Bei diesen Pässen gibt es 4 Möglichkeiten:  
ein normaler Pass der den Ball einfach da hinbringt wo die Pfeilspitze ist, ein Dribbling (man kann eine Handkarte des Gegners ziehen), ein Starspieler (eine Entscheidungskarte wird umgedeckt um zu prüfen ob man nochmal dran ist) und ein Foul (wieder eine Entscheidungskarte umdecken um zu prüfen ob das Foul geahndet wird -> Freistoß + ggf Gelbe und Rote Karte)  
Außerdem gibt es noch die Möglichkeit wenn der Ball sich auf einer der drei Positionen in der gegnerischen Hälfte befindet aufs Tor zu schießen. Dabei wird wieder eine Entscheidungskarte umgedeckt um zu schauen ob man ein Tor geschoßen hat, Alternativen dazu sind Abstoß, Eckball oder Pfostentreffer.  
![4 Spielerkarten](/img/wannabefootball-Spielerkarten.jpg "Beispiele der Spielerkarten:")
![bla](/img/wannabefootball-entscheidungskarten.jpg "Beispiele der Entscheidungskarten:")

Wenn der Stapel mit den Spielerkarten leer ist wird noch fertig gespielt und dann ist Abpfiff. Wie beim Fussball üblich gewinnt diejenige mit den meisten Toren. 

### Was gefällt mir?
* kurzweiliges, schnelles Kartenspiel das ein Fussballspiel ganz gut nachempfindet
* Die Schachtelgröße passt perfekt für die beiden Kartenstapel
* Lustige Illustrationen mit verschiedenen Hautfarben
* einfach Regeln
* gutes Regelvideo
* Sprachneutrale Karten

### Was gefällt mir nicht?
* Auch wenn die Regeln eigentlich einfach sind werden sie durch die Spielanleitung kompliziert gemacht. Ohne das Regelvideo sind die Regeln leider nicht eindeutig zu verstehen
* Wäre cool gewesen auch Frauen auf den Karten darzustellen

