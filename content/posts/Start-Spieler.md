---
author: "me"
title: "Vorstellung: Start Spieler"
date: "2022-04-20"
description: ""
tags: ["Vorstellung"]
ShowToc: false
#draft: true
cover:
    image: "/img/startspieler-vorderseite.jpeg"
    alt: "Spieleschachtel Startspieler"
    hiddenInList: true
info:
    verlag: "Bewiched Spiele"
    verlaglink: "https://www.bewitched-spiele.de" 
    autor: "Ted Alspach"
    grafik: "Sebastian Wagner"
    alter: "ab 6 Jahren"
    dauer: "unter 1 Minute"
    spieler: "2-unendlich"
    kosten: "5,99€"
---
### Was ist das eigentlich?
Ein Satz Karten (53) und je eine Zusatzkarte für die "Spielregeln" und den Verlag. Damit lässt sich in handumdrehen herausfinden wer beim nächsten "richtigen" Spiel Startspielerin ist. Denn auf jeder Karte steht eine Regel wie die Startspielerin ermittelt werden kann, für den Fall eines Gleichstandes ist zudem noch ein Pfeil aufgedruckt, nach dessen Ausrichtung dann entschieden wird.

![Startspieler drei Karten](/img/startspieler-spieleindruck.jpeg)

### Meine Erlebnisse mit dem Spiel.  
Eigentlich ist es ja kein richtiges Spiel aber irgendwie auch schon; aber von vorne: Die Idee mehrere Karten mit verschiedenen Startspielerregeln zu haben hab ich zum ersten mal bei [Between to Cities](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=29183) gesehn. Dort liegen einige Karten mit in der Schachtel. Nachdem ich die immer mal wieder gesammelt und aufgeschrieben habe es aber nie wirklich geschafft habe selbst was zu basteln, hab mich doch auf die Suche nach einem gekauften Deck gemacht. Und bin mit Startspieler von Ted aspach fündig geworden.

### Was finde ich so toll:
* viele kreative Ideen für Startspielerregeln, durch die man sich auch bessser kennenlernen kann
* die Grafik ist total liebevoll gemacht vorallem auch mit den comics auf jeder Karte
