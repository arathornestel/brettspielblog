---
author: "me"
title: "Neu im Regal [4/21]"
date: 2022-01-08T12:01:19+01:00
description: "Neuzugänge und Abgänge, sortiert nach Grundspiel und Erweiterung aus dem letzten Quartal."
tags: ["neu im Regal"]
ShowToc: false
---
### Neue Spiele
* [Fantastische Reiche](https://brettspielrat.de/fantastische-reiche)
* [Gold](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=33877)
* [My Gold Mine](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=36328)
* Calico
* Bios:Megafauna
* Chronicles of Crime - 2400
* Micro Macro Crime City - Fullhouse
* Dungeon Twister
* The Mind
* Wannabe Football
* [Robin Hood](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=36124)
* IceCool
* Panic Mansion
* [Yokai](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=33851)
* Anno Domini - Frauen
* Romeo & Julia
* Cosmogenesis
* Android Mainframe
* Hope

#### Erweiterungen
* Tsukuyumi - Moon Princess
* Fantastische Reiche Erweiterung
* Dungeon Twister Erweiterungen 1,2 und 4

### Ausgezogen
* Herr der Ringe, Das Kartenspiel - Die Gefährten
* Herr der Ringe, Das Kartenspiel - Die zwei Türme
* Mexica
* Dschunke
* Atzlan
* Tikal
* Taj Mahal
* Cartagena
* Fog of Love