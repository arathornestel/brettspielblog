---
author: "me"
title: "Rückblick KW20"
date: 2022-05-25T11:48:12+02:00
description: ""
tags: ["Rückblick","gespielt"]
ShowToc: false
cover:
    image: "/img/cover-rückblick-kw20.jpeg"
    # can also paste direct link from external site
    # ex. https://i.ibb.co/K0HVPBd/paper-mod-profilemode.png
    alt: "<Der Stapel der Spiele die im Rückblick Thema sind>"
    caption: "<text>"
    hidden: true
---
In der letzten Woche habe ich das neue [Robo Chaos](https://boardgamegeek.com/boardgame/356133/quirky-circuits-penny-gizmos-snow-day) zumindest ein zwei Runden mal angespielt und es ist auf jedenfall vielversprechend ;)  

Dann war ich am Wochenende in einer Hütte auf der Schwäbischen Alb und dort haben wir zu sechst einige Runden [Lucky Lachs](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=31519) gezockt. Super simpel und richtig schnell gespielt - der perfekte Energizer. Von außen betrachtet sieht es folgendermaßen aus: "eine Gruppe Menschen steht brüllend im Kreis, werfen Karten auf den Boden, vollführen verschiedene Checks und wechseln ab und an die Plätze."  
<!--more-->
Am Sonntag Abend hatten wir dann noch nen Spieleabend mit nem Kumpel. Dabei haben wir [Age of Civilization](https://boardgamegeek.com/boardgame/264647/age-civilization) und [Ratland](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=33059) gespielt. Ersteres ist ein schnelles (ca 30 Minuten) Zvilisationsaufbau-Spiel:  
![Bild: Age of Civilization Spieleindruck](/img/ageofciv-spieleindruck.jpg)
Bei Ratland spielen wir einen Rattenclan der versucht sich zu vermehren und dafür Käse organisieren muss. Das geht sowohl über die Suche an der Oberfläche als auch mit Überfällen auf die benachbarten Clans.