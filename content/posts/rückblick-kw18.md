---
author: "me"
title: "Rückblick Kw18"
date: 2022-05-08T23:45:46+02:00
#draft: true
description: "Rückblick auf die Spiele die ich letzte Woche gespielt habe, sowie neu angekommene Spiele und was sonst so passiert ist."
cover:
    image: "/img/cover-rückblick-kw18.jpeg"
    # can also paste direct link from external site
    # ex. https://i.ibb.co/K0HVPBd/paper-mod-profilemode.png
    alt: "<Der Stapel der Spiele die im Rückblick Thema sind>"
    caption: "<text>"
    hidden: true
tags: ["Rückblick","gespielt"]
ShowToc: false
---
Lezte Woche bestand vorallem aus Spieleabend bei nem Kumpel am Freitag. Wir haben [Dungeon Twister](https://luding.org/Skripte/GameData.py/DEgameid/19313), [Wannabe Football](https://luding.org/cgi-bin/GameName.py?f=00w%5EE4X&gamename=Wannabe+football) und [Galaxy trucker](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=27047) gespielt.  
![Spieleindruck Dungeon Twister](/img/dungeon-twister_spieleindruck.jpg)
Außerdem habe ich mir nach einer Empfehlung vom Spiel des Monats von Betterboardgames, das Spiel [Lantris](https://www.luding.org/cgi-bin/GameName.py?f=00w%5EE4X&gamename=lantris) gekauft. Das ich gestern beim stadtbummel mit meiner Freundin auch gleich ausprobiert hab. Taugt mir sehr gut, für mein Freundin ist zu viel räumliches Denken enthalten.  
![Spieleindruck Landris](/img/landris_spieleindruck.jpeg)
An der gleichen Stelle haben wir dann auch mal wieder [Fantastische Reiche](/fantastische-reiche) gespielt.  
![Handkarte am Ende von Fantastische Reiche](/img/fantastische-Reiche_Handkarten.jpeg)
Und dann gibts noch zwei Neuerungen am Blog, zum einen gibt es jetzt eine [Über mich](/ueber-mich) Seite und ich habe den Mastodon-Account von social.tchncs.de zu rollenspiel.social umgezogen.