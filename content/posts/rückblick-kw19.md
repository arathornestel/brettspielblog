---
author: "me"
title: "Rückblick KW19"
date: 2022-05-18T08:13:20+02:00
description: ""
tags: ["Rückblick","gespielt"]
ShowToc: false
---
Letzte Woche sind bei mir drei neue Spiele angekommen:
* [Mechs vs Minions](https://www.luding.org/cgi-bin/GameData.py/DEgameid/31113)
* [Die Tiere vom Ahorntal](https://www.luding.org/cgi-bin/GameName.py?f=00w%5EE4X&gamename=Die+Tiere+vom+Ahorntal)
* [Robo Chaos](https://boardgamegeek.com/boardgame/356133/quirky-circuits-penny-gizmos-snow-day)
  
Mechs vs Minions ist vom ersten Blick her eine rießige Schachtel! Aber tatsächlich ist das Spiel an sich dann gar nicht mal so kompliziert und dauert auch nur so etwa eine Stunde. Hab das mit Freundinnen von mir am Wochenende auch direkt gezockt und bin sehr überzeugt von meiner Kaufentscheidung ;) Im Spiel programmiert man Mechs um sie gegen Feinde einzusetzen oder zu bestimmten Orten auf der Karte zu gelangen - dabei ist das ganze kooperativ und es gibt 10 Missionen die aber auch unabhängig von einander gespielt werden können.
![Aufbau erste Mission](/img/MechsvsMinions_spieleindruck.jpeg)  
Die anderen beiden Spiele konnte ich noch nicht ausprobieren, daher da erst in den nächsten Wochen mehr dazu.   
Aber an besagtem Wochenende konnten wir auch noch zu dritt das Spiel [Alone](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=34075) ausprobieren das ne Freundin sich gekauft hat. Man spielt gegeneinander, wobei eine die Heldenspielerin steuert die versucht sich in einer verlassen Raumstation zurechtzufinden und Missionen zu erledigen. Die anderen Spielerinnen spielen zusammen die Raumstation mit all ihren Monstern und Fallen und versuchen die Heldenspielerin nach Möglichkeit zu behindern und letztendlich davon abzuhalten ihre Missionen zu erledigen. War relativ komplex aber hat Spaß gemacht und ich denke nach ein zwei Partien sollte es auch flüssiger laufen. 
![Heldenspieler im Spiel](/img/alone_spieleindruck.jpeg) 
Außerdem hab ich mit meiner Freundin das vorletzte Kapitel von [Robin Hood](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=36124) gespielt und muss leider sagen, das wir es langsam anstrengend finden. So richtig vom Hocker reißen tut die Story leider nicht mehr und auch wenn der Mechanismus echt spannend ist kann er darüber nicht hinweghelfen. Vermutlich spielen wir noch das letzte Kapitel und werden das Spiel dann entweder ganz weggeben oder verleihen.  
An besagtem Wochenende stand dann zusätzlich noch [Magic Maze](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=31690) und [Fort](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=36598) auf dem Programm.