---
author: "me"
title: "Vorstellung: Power Plants"
date: "2023-05-07"
description: ""
tags: ["Vorstellung"]
ShowToc: false
url: /power plants
cover:
    image: "/img/cover-Power_Plants.jpg"
    alt: "Coverimage Spiel Power Plants"
    hiddenInList: true
info:
    verlag: "Skellig Games"
    verlaglink: "https://www.skellig-games.de/" 
    autor: "Adam E. Daulton"
    grafik: "Apolline Etienne"
    alter: "ab 8 Jahren"
    dauer: "30 Minuten"
    spieler: "2-5"
    kosten: "34,90 €"
---
### Was sagt der Verlag?
In Power Plants seid ihr Zauberer und platziert Pflanzen und Feen in einem Zaubergarten. Dabei müsst ihr euch in jedem Zug entscheiden, ob ihr neue Pflanzen im Garten anpflanzt und dadurch eine starke Aktion nutzt oder ob ihr bereits bestehende Pflanzen weiter wachsen lasst und dadurch bis zu vier schwächere Aktionen nutzen wollt.
### Meine Erlebnisse mit dem Spiel. 
Ich war in der Kickstarter-Kampagne. Letzten Herbst wars dann soweit und das Spiel kam bei mir an. Da ich inzwischen einige Runden spielen konnte, kann ich euch auch sagen was ich von halte.
### Wie funktionierts?
Vor Beginn der Partie werden aus 8 verfügbaren Pflanzen 5 ausgewählt. Daraufhin wird je ein Plättchen(Beet) der ausgewählten Pflanzen in die Mitte gelegt. Dies ist der Beginn des Gartens. Jede Spielerin zieht aus einem Beutel 2 Plättchen und das Spiel beginnt.
Jede Runde wird ein Plättchen an den Garten angelegt und die aktive Spielerin wählt ob sie die eine Starke Aktion des gerade angelegten Beetes nutzt oder die schwächeren Aktionen aller an das angelegte Beet angrenzenden Beete.  
Durch die Aktionen werden Feen und Edelsteine im Garten platziert, um möglichst große Felder, bestehend aus mehreren zusammenhängenden Beeten der gleichen Pflanze zu kontrollieren. Das gibt am Ende auch die Siegpunkte. Außerdem bekommt man die Edelsteine die auf den Beeten verteilt sind die man kontrolliert und man kann während der Partie durch Aktionen Edelsteine direkt für sich gewinnen. Da Feen grundsätzlich von anderen Feen verdrängt werden entsteht ein rasantes hin und her um die begehrtesten Beete.
![Spielbrett am Ende der Partie](/img/spieleindruck-PowerPlants1.jpg)
### Was finde ich so toll:
* Ziemlich gut vollgepackte Schachtel wobei in der Retail-Version noch etwas mehr Platz sein dürfte.
* tolles Holzmaterial (Münzen, Feen, Beetplättchen)
* Sehr abwechslungsreich durch die verschiedenen Fähigkeiten der Pflanzen und die immer neue Anordnung.  Außerdem gibt es auch noch je Farbe eine komplexe Version der Pflanzen.
### Was ich nicht so toll finde:
* Die Farben sind nicht ganz easy auseinander zu halten da wäre eine etwas deutlichere Symbolik hilfreich gewesen
* Die Detailkarten der Pflanzen die ausgelegt die reihenfolge der schwachen Aktion bestimmen sind ziemlich groß. Dadurch verdoppelt sich der Platzbedarf auf dem Tisch.
