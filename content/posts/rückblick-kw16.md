---
author: "me"
title: "Rückblick KW16"
date: 2022-04-26T17:34:19+02:00
description: ""
cover:
    image: "/img/cover-rückblick-kw16.jpeg"
    # can also paste direct link from external site
    # ex. https://i.ibb.co/K0HVPBd/paper-mod-profilemode.png
    alt: "<Der Stapel der Spiele die im Rückblick Thema sind>"
    caption: "<text>"
    hidden: true
tags: ["Rückblick","gespielt"]
ShowToc: false
---
Letzte Woche konnte ich leider nur zwei Spiele spielen.
Zunächst Montag abend mit meiner Freundin [Parks](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=33744). Hat mich leider nicht wirklich überzeugt; meine Freundin dagegen hatte viel Spaß damit. Das Material ist zwar sehr schön, insbesondere auch die Illustrationsreihe der Nationalparks. Allerdings fehlt für mich der spielerische Reiz.  
Am Samstag abend hatten wir dann einen großen Spieleabend mit Freunden zu fünft. Gespielt haben wir [das Streben nach Glück](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=33571). Hat mich gefreut das mal wieder auf den Tisch zu bekommen. Gefällt mir sehr gut, mit vielen Karten zum entdecken. Als Beschreibung könnte man sagen es ist so ähnlich wie Sims als Brettspiel aber ohne Haus bauen und mit nur einem Character den man steuert.  
Freu mich auch schon rießig auf die BigBox von der Spieleschmiede die vermutlich gegen Weihnachten ausgeliefert wird ;).  
  
Außerdem spiele ich gerade außerhalb der analogen Welt noch [Hades Revisited](https://content.minetest.net/packages/Wuzzy/hades_revisited/), ein Subgame von [Minetest](https://www.minetest.net/). Im Spiel ist man auf einem Lavaplaneten gestrandet und versucht diesen mithilfe von Terraforming in eine grüne Oase zu verwandeln. Super spannender Ansatz wie ich finde. Aktuell ist das Spiel allerdings noch im Alpha-Stadium. Aber spielt sich trotzdem schon recht cool ;)
![spieleindruck von Hades](/img/hades_revisited_spieleindruck.png)