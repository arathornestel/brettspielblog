---
author: "me"
title: "Vorstellung: Yggdrasil Chronicles"
date: "2023-09-06"
description: ""
tags: ["Vorstellung"]
ShowToc: false
url: /yggdrasil-chronicles
#draft: true
cover:
    image: "/img/cover-yggdrasil.png"
    alt: "Coverimage Spiel Fantastische Reiche"
    hiddenInList: true
info:
    verlag: "Ludonaute"
    verlaglink: "https://www.ludonaute.fr/"
    autor: "Cédric Lefebvre"
    grafik: "Maëva Da Silva und Christine Deschamps"
    alter: "ab 14"
    dauer: "ca. 90 Minuten"
    spieler: "1-5"
    kosten: "50€"
---
### Das sagt der Verlag
Yggdrasil Chronicles ist ein fesselndes und fesselndes kooperatives Spiel mit verschiedenen Spielmodi, darunter einer Kampagne mit 6 wiederspielbaren Szenarien, einem 2-Spieler-Modus und einem Einzelspieler-Modus. Spieler verkörpern die Götter und bereisen den Yggdrasil-Weltbaum, um ihre Feinde abzuwehren. Um diese Geißeln auszurotten, die das Leben der kosmischen Esche bedrohen, können sie auf die Unterstützung der nordischen Völker und Kreaturen, ihre Fähigkeiten und die Kraft ihrer göttlichen Artefakte zählen. Aber noch mehr: Ihre Zusammenarbeit und ihre Beziehungen werden es ihnen ermöglichen, die Treulosigkeit von Surt, Hel, Iormungand, Fenrir, Loki und Nidhögg zu überwinden. Sind Sie bereit für ein Eintauchen in das Pantheon der nordischen Mythologie?
### Wie funktionierts?
Jede Spielerin übernimmt die Rolle eines Gottes. In der schwierigen Standard-Partie und der Kampagne haben diese leicht unterschiedliche Zusatzfähigkeiten.
Zu Beginn einer Runde legt jede eine Feindkarte verdeckt auf das "Rad der Feinde". Die Spielerinnen sind in beliebiger Reihenfolge dran. Wer am Zug ist deckt als erstes seine Feindkarte auf. Sobald zwei gleiche offen liegen wird der Feindeffekt ausgeführt. Falls dabei zwei Feinde in einer Welt stehen wird diese zerstört. Nach der Ausführung des Feindeffekts kann die aktiven Spielerin ihren Gott in eine angrenzende Welt bewegen (optional). Danach kämpft sie gegen einen anwesenden Feind oder führt eine Welten- oder Kampagnenaktion aus.
Dies wird über mehrere Runden wiederholt bis die Spielerinnen entweder gewonnen (nach einer durch den Schwierigkeitsgrad oder die Kampagne bestimmten Anzahl Runden) oder verloren (Wenn ein Feindeffekt nicht mehr ausgeführt werden kann, ein Gott kein Leben mehr hat oder alle Welten der Esche zerstört sind).
Im Fall der Kampagne werden danach noch XP verteilt die für zusätzliche Fähigkeiten ausgegeben werden können.
### Meine Erlebnisse mit dem Spiel. 
Das Spiel habe ich in einem Youtube-Video gesehen und dann günstig bei Kleinanzeigen gefunden. Leider war es ein total verrauchtes Exemplar. Mit getrocknetem Kaffeesatz bin ich den Geruch aber weitesgehend losgeworden. Was ein Glück ist denn das Spiel selber gefällt mir richtig gut ;).
Aktuell spiele ich mit einem Kumpel, nach einer gewonnenen einfachen Standardpartie die Kampagne, wobei wir in drei Versuchen bisher immer verloren haben. Spaß haben wir aber trotzdem.
### Was gefällt mir
* die Tischpräsenz der Weltenesche ist einfach der Hammer und im Gegensatz zu z.B. Everdell ist hier der Baum auch nehr als nur hübsche Deko.
![Spielbrett am Ende der Partie](/img/spieleindruck-yggdrasil.jpg)
* Coole Extra-Elemente über die Kampagne.
* das Thema nordische Mythologie ist stimmig umgesetzt und holt mich halt einfach voll ab ;)
* der Zweispieler Modus simultiert den dritten Gott auf einem einfachen Level. d.h. ohne großen Verwaltungsaufwand
### Was gefällt mir nicht
* der Zusammenbau des Baumes ist nicht ganz easy, da nicht intuiv klar ist was wohin gehört.