---
author: "me"
title: "Rückblick KW 15"
date: 2022-04-18T16:56:03+02:00
description: ""
tags: ["gespielt", "neu im Regal", "Wochenrückblick"]
cover:
    image: "/img/cover-rückblick-kw15.jpeg"
    # can also paste direct link from external site
    # ex. https://i.ibb.co/K0HVPBd/paper-mod-profilemode.png
    alt: "<Der Stapel der Spiele die im Rückblick Thema sind>"
    caption: "<text>"
    hidden: true
ShowToc: false
---
Letzte Woche habe ich eine weitere Runde [7 Wonders Duel](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=28657) mit meiner Freundin gespielt. Gefällt mir richtig gut. Einfach und schnell erklärt, mit einer angenehmen taktischen tiefe und trotzdem in 30 bis 45 Minuten gespielt.  
Außerdem haben wir noch das dritte Kapitel von [Robin Hood](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=36124) gespielt. Vergleichbar mit [Andor](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=25277), aber es erreicht meiner Meinung nach eine breitete Zielgruppe und ist auch etwas zugänglicher.  
Samstag waren wir dann im [Spielespatz](https://spielespatz.de/) in Ulm, meinem Lieblings-Spieleladen. Sehr gemütlich und man kann sogar met kaufen ;) Dort haben wir uns mit [Galaxy Trucker](https://boardgamegeek.com/boardgame/336794/galaxy-trucker) (2te Edition), eines meiner Lieblingspiele und außerdem noch [Fort](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=36598) gegönnt. Das haben wir abends dann auch gleich ausprobiert. Gefällt uns gut; superschöne Grafik und durch die symbolübersicht sind auch die Aktionen schnell erklärt. 
![Spieleindruck von Fort](/img/Fort-spieleindruck.jpg)
Außerdem haben wir am Sonntag dann noch eine Runde [Siedler von Catan](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=1508) gespielt. Die erste seit vermutlich 5 jahren :D  Ist immer noch schön zugänglich, macht Spass einziges Manko durch das viele Würfelpech gibt es immer wieder viele Runden in denen nichts passiert.