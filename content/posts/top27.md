---
author: "me"
title: "TOP 27 aktuell"
date: "2021-12-08"
description: "Meine liebsten 27 Brettspiele"
tags: ["toplisten"]
ShowToc: true
#draft: true
---
Direkt als zweiten Post eine große Topliste, damit ihr wisst was ich mag. Ich hoffe das hilft euch besser einzuschätzen wie ihr mit meinen Reviews umgehen könnt.
Die Liste habe ich mithilfe eines Rankingtools erstellt, was ich recht spannend zum ausprobieren fand. Zumal es ja nicht darum geht ob jetzt was auf Platz 1 oder 4 rauskommt sondern eher um den Gesamteindruck. Die meisten weiterführende Links zu den Spielen führen auf luding.org einzelne auch auf boardgamegeek. Und jetzt viel Spaß mit der Liste!


|Platz|Titel|Autor|Erscheinungsjahr|
|---|---|---|---|
|1|[Galaxy Trucker](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=27047)|Vladimír Chvátil|2007|
|2|[Blood Rage](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=28648)|	Eric M. Lang|2015|
|3|[Reise durch Mittelerde](https://www.luding.org/cgi-bin/GameName.py?f=00w%5EE4X&gamename=Reise+durch+Mittelerde)|Nathan Hajek & Grace Holdinghaus|2019|
|4|[Der Ringkrieg: 2te Edition](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=25262)|Marco Maggi & Francesco Nepitello & Roberto di Meglio|2012|
|5|[Tsukuyumi](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=30251)|Felix Mertikat|2016|
|6|[Fantastische Reiche](https://www.luding.org/cgi-bin/GameName.py?f=00w%5EE4X&gamename=fantastische+Reiche)|Bruce Glassco|2020|
|7|[Das Streben nach Glück](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=33571)|Adrian Abela & David Chircop|2019|
|8|[Wasserkraft](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=33942)|Simone Luciani & Tommaso Battista|2020|
|9|[Bios: Genesis](https://www.luding.org/cgi-bin/GameName.py?f=00w%5EE4X&gamename=bios+genesis)|Phil Eklund|2016|
|10|[Era of Tribes](https://www.luding.org/cgi-bin/GameName.py?f=00w%5EE4X&gamename=Era+of+tRIBES)|Arne Lorenz|2019|
|11|[Die Crew: Mission Tiefsee](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=36131)|Thomas Sing|2021|
|12|[Pandemie](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=21947)|Matt Leacock|2008|
|13|[RoboRally](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=19328)|Richard Garfield|2005|
|14|[Trick Shot](https://www.boardgamegeek.com/boardgame/256707/trick-shot)|Artyom Nichipurov|2021|
|15|[Age of Civilization](https://www.boardgamegeek.com/boardgame/264647/age-civilization)|Jeffrey CCH|2019|
|16|[Crave](https://www.boardgamegeek.com/boardgame/264314/crave)|Bryan Sloan|2019|
|17|[Windward](https://www.boardgamegeek.com/boardgame/282922/windward)|Hayden Lapiska & Daniel Aronson & Nick Tompkins|2020|
|18|[Pulsar 2849](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=32371)|Vladimir Suchy|2017
|19|[Adrenalin](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=31275)|Filip Neduk|2016
|20|[Lovecraft Letter](https://www.luding.org/cgi-bin/GameName.py?f=00w%5EE4X&gamename=lovecraft+letter)|Seiji Kanai|2016
|21|[Feierabend](https://www.luding.org/cgi-bin/GameName.py?f=00w%5EE4X&gamename=Feierabend)|Friedemann Friese|2020
|22|[Ratland](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=33059)|Eduardo García Martín|2017
|23|[Mysterium](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=28437)|Oleksandr Nevskiy & Oleg Sidorenko|2015
|24|[Charterstone](https://www.luding.org/cgi-bin/GameName.py?f=00w%5EE4X&gamename=Charterstone)|Jamey Stegmaier|2017
|25|[Die Zwerge](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=25430)]|Michael Palm & Lukas Zach|2012
|26|[Magic Maze](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=31690)|Kasper Lapp|2017
|27|[Detective](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=32892)|Ignacy Trzewiczek|2018
---
Die Liste habe ich mtihilfe von https://rankingengine.pubmeeple.com/ erstellt.
