---
author: "me"
title: "Vorstellung: Paris - Die Stadt der Lichter"
date: "2023-03-27"
description: ""
tags: ["Vorstellung"]
ShowToc: false
url: /paris
cover:
    image: "/img/cover-paris.jpg"
    alt: "Coverimage Spiel Paris - Die Stadt der Lichter"
    hiddenInList: true
info:
    verlag: "Kosmos"
    verlaglink: "https://www.kosmos.de/" 
    autor: "José Antonio Abascal Acebo"
    grafik: "Oriol Hernández"
    alter: "ab 10 Jahren"
    dauer: "30 Minuten"
    spieler: "2"
    kosten: "19,99 €"
---

### Was sagt der Verlag?
Paris im Jahr 1889. Pünktlich zur Exposition Universelle zeigt die Stadt an der Seine der ganzen Welt ihre neueste Errungenschaft: die elektrische Straßenbeleuchtung. Und zwei Spieler sind bei dem historischen Ereignis live dabei! Sie schlüpfen in diesem Strategiespiel in die Rollen zweier konkurrierender Baumeister.  
In zwei Phasen lassen sie ein neues Viertel entstehen: Sie nehmen sich Straßen vor, planen Gebäude und errichten prächtige Bauwerke. Dabei muss jeder Spieler versuchen, seine Aktionsplättchen so lukrativ wie möglich einzusetzen. Um die meisten Punkte zu ergattern, müssen die Bauwerke im Glanze der Lichter erstrahlen.  
### Meine Erlebnisse mit dem Spiel.  
Gekauft hatte ich das Spiel schon vor 2 Jahren allerdings ist es dann erstmal nicht wirklich auf den Tisch gekommen. In den letzten paar Wochen konnte ich aber enige Partien mit verschiedenen Menschen spielen, und es hat überzeugt ;)  
### Was finde ich so toll:
* Richtig gut gefällt mir die Optik. Es baut sich nach und nach ein richtig schönes Bild auf und die Rückseiten der Aktionspostkarten sind eigentlich Kunstwerke.   
* Die Regeln sind relativ leicht und durch die Aufteilung in zwei Phasen kann man sich auch gut in Etappen rantasten.  
Sobald man dann drinn ist wird es richtig schön strategisch.
* Die Box ist ziemlich cool gemacht und trägt zur eindrucksvollen Tischpräsenz bei.
![Spielbrett am Ende der Partie](/img/spieleindruck-paris1.jpg)
### Was ich nicht so toll finde:
* Die Aktionspostkarten sind eigentlich zu groß und wenn man sie wir vorgeschlagen um die Schachtel rumplaziert kann man eigentlich nicht alle Aktionen überblicken
* Das zweite Manko gilt auch den Postkarten - allerdings nur so halb: Die sind eigentlich zu hübsch und man sieht Sie während dem Spiel gar nicht so richtig ;(
