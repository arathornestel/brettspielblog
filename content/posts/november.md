---
author: "me"
title: "Gespielt im November"
date: "2021-12-07"
description: ""
schlagworte: ["gespielt"]
ShowToc: true
#featured_image: "/img/cover-gespielt-november-bearbeitet.jpg"
cover:
    image: "/img/cover-gespielt-november.jpg"
   # can also paste direct link from external site
   # ex. https://i.ibb.co/K0HVPBd/paper-mod-profilemode.png
    alt: "<Coverimage Gespielt im November>"
    caption: "<text>"
    relative: false # To use relative path for cover image, used in hugo Page-bundles
    hidden: true
---

So, diesmal der erste richtige Inhalt...
Letzten Monat ging eigentlich ziemlich viel. Meine Highlights:
* Fantastische Reiche, Ein Drafting-Spiel bei dem jede Mitspielerin 7/8 Karten (je nachdem ob mit Erweiterung) bekommt. Jede Runde wird eine Karte gezogen (vom Stapel oder aus der Auslage) und eine Karte in die Auslage abgelegt. Das Spiel endet sobald 10/12 Karten (je nachdem ob mit Erweiterung) in der Auslage liegen hat. Gewonnen hat wer die beste Kombination an Karten hat. Denn wir bauen uns ein Reich aus verschiedene Kartenkategorien wie Länder, Armeen Anführer usw. auf. Aus jeder Karte gibt es 5 verschiedene Karten die, wenn sie gemeinsam auf der Hand sind sich gegenseitig Boni und Strafen geben. Außerdem hat jede Karte noch eine Basisstärke.
* Wannabe Football, ein schnelles Sportspiel bei dem wir gegeneinander ein Fussball-Match austragen. Dazu nutzen wir Karten die immer abwechselnd auf einen Stapel gelegt werden. Auf diesen sind aktuelle und zukünftige Position des Balls vermerkt, außerdem gibt es natürlich auch Torschusskarten. Beim Torschuss und anderen Zufallselementen wird eine Karte vom zweiten Stapel umgedeckt um zu schauen ob man ein Tor erzielt hat.

weitere Titel:
* Die Crew - Mission Tiefsee
* City Council
* Crave
* Canasta
* Lovecraft Letter
* Sea Change

online:
* Hive
* Pandemic - Hot Zone: North America
* Clash of Decks
