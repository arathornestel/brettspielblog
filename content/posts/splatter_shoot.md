---
author: "me"
title: "Kommentar: Wie viel Spiele sind zu viel?"
date: "2023-06-26"
description: ""
tags: ["Kommentar"]
ShowToc: false
draft: true
---
In letzter Zeit frage ich mich öfter Mal ob ich eigentlich zu viele Spiele habe. Insbesondere durch die Pandemie wurde bei mir die Bedeutung von Spiele entdecken und dann teilweise auch kaufen verstärkt. Irgendwie Paradox, da auf der einen Seite bin ich nicht meht so viel zum spielen kommen auf der anderen Seite habe ich mehr Spiele als je zu vor gekauft. Meine Vermutung: Reviews lesen bzw. anschauen und dadurch auch Spiele kaufen war die einzige Möglichkeit das Hobby auszuleben. Auch habe ich mir einige Tabletop- sowie Rollenspiel-Systeme angeschaut und auch gekauft. Auch hier mehr als ich gespielt habe.