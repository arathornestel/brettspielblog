---
author: "me"
title: "Hallo Welt"
date: "2021-12-06"
description: ""
tags: [""]
ShowToc: true
---

So, das werden jetzt die ersten paar Zeilen. Gleich vorneweg Rechtschreibung ist nicht unbedingt meine Stärke aber ich hoffe ihr werdet's mir verzeihen.
Ich weiß noch nicht genau was ich hier schreiben werde aber ein paar Ideen hab ich:
* Monatliche Rückblicke was ich so gespielt habe
* Quartalsmäßige Berichte über neue Spiele und Spiele die wieder gehen müssen
* Bastelberichte: Boxenumbau's, Inserts, Geländebau sowie bemalte Mini's und mehr...
* Exkurse in Themen wie Klimagerechtigkeit, IT-Security/Opensource/Datenschutz und Stadtplanung
