---
author: "me"
title: "Rückblick KW 13"
date: 2022-04-03T21:03:25+02:00
draft:
description: ""
schlagworte: ["gespielt","Wochenrückblick"]
ShowToc: false
---
Letzte Woche konnte ich zwei neue Spiele ausprobieren.
Zum einen [Wasserkraft](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=33942) von Tommaso Battista und Simone Luciani, zum anderen 7 Wonders Duel.  
Beides zu zweit mit meiner Freundin. Wasserkraft kannte ich schon von einer Online-Runde auf Tabetopia und ich bin nach wie vor sehr begeistert davon. Meiner Freundin hat es leider nicht so getaugt. Dafür sind wir beide sehr gut mit [7 Wonders Duel](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=28657) zurechtgekommen, wovon ich in den letzten Wochen und Monaten immer mal wieder begeisterte Reviews gelesen habe.  
Außerdem ist endlich [Blood Rage](https://www.luding.org/cgi-bin/GameData.py?f=00w^E4X&gameid=28648) angekommen was ich mir schon vor nem Monat oder so gebraucht bei Ebay Kleinanzeigen gegönnt habe. Das Spiel hatte ich auf meiner ersten Spiel in Essen ausprobiert, seither aber immer wieder aus den Augen verloren und auch nicht mehr gespielt. Das wird nächste Woche auf jeden Fall noch ausprobiert ;)
![Bloodrage](/img/Bloodrage.jpg)