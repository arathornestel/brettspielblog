---
author: "me"
title: "Bastelprojekt: horizontal chem tank"
date: "2022-03-29"
description: ""
tags: ["basteln","tabletop"]
ShowToc: false
#draft: true
---

Ich hatte den Bausatz vor inzwischen bestimmt 3 Jahren im Shop von [EveryLittleWar](https://everylittlewar.com/collections/promethium-forge/products/horizontal-chem-tank-kit) gekauft. Jetzt habe ich es endlich geschafft alles zusammenzubauen und zu bemalen. Von der Firma Protheum Forge gibt es noch weitere Bausätze die alle gemein haben das man sie als Basis, ein oder mehrere Getränkedosen haben.

In meinem Fall habe ich JimBeam WhiyskeyCola-Dosen verwendet. Ich hoffe mal die Auswirkungen auf die Umwelt halten sich in Grenzen bei zwei gekauften Dosen :D

![Rohbau](/img/chemtank_rohbau.jpg)

Das zusammenbauen funktionierte einfach mit Holzleim. Für die Bemalung habe ich ganz normale Acrylfarbe aus dem Baumarkt bzw Bastelfachmarkt genommen.
Für den Rosteffekt habe ich die Schwamm-Tupf-Technik von [Wylochs Army](https://invidio.xamh.de/watch?v=S07hHjXgd60&autoplay=0&continue=0&dark_mode=true&listen=0&local=1&loop=0&nojs=0&player_style=youtube&quality=hd720&thin_mode=false) eingesetzt und bin damit ziemlich happy ;)

![Fertig bemalt](/img/bemalterTank.jpg)

Und anschließend habe ich dem Tank dann noch eine hübsche Bodenplatte spendiert.

![Tank mit Bodenplatte](/img/bemalterTankmitBase.jpg)