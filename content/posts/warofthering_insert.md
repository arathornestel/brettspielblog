---
author: "me"
title: "Bastelprojekt:War of the Ring Insert"
date: 2022-10-06T17:53:22+02:00
description: ""
tags: [""]
ShowToc: false
---
Dank Corona-Isolation bin ich endlich mal das Projekt für ein Insert von War of the Ring angegangen.  
Das Insert ist ein Bausatz von Folded Space bestehend aus "Architektenpappe" (Foamcore). Schneiden muss man dabei nichts, sondern einfach nur die Teile aus den Stanzrahmen trennen und zusammenkleben. Ging recht schnell und überwiegend gut von der Hand.
Sobald ich auch die Erweiterungen vollständig reingepackt habe kommt nochmal ein Mini-Update.
Außerdem hab ich quasi anschließend auch verstärkt das Projekt die ganzen Miniaturen anzumalen in Angriff genommen.
![Bausatz fertig gebastelt](/img/waroftheringinsert_fertiger_bausatz.jpeg)
![Bausatz fertig gebastelt](/img/waroftheringinsert_leer.jpeg)
![Bausatz fertig gebastelt](/img/waroftheringinsert_befüllt.jpeg)