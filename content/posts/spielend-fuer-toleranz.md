---
author: "me"
title: "Spielend für Toleranz"
date: "2021-12-18"
description: ""
tags: [""]
ShowToc: false
weight: 2
#draft: true
cover:
    image: "/img/Spielend-fuer-Toleranz.jpg"
    # can also paste direct link from external site
    # ex. https://i.ibb.co/K0HVPBd/paper-mod-profilemode.png
    alt: "<Logo der Aktion Spielend für Toleranz>"
    caption: "<text>"
    hidden: true
---
Nachdem ich das Logo immer wieder gesehn habe, dachte ich mir ich möchte diese Aktion auch unterstützen und ein Zeichen setzen.  
Aus meiner Sicht ist das eines der Dinge die ich an Brettspielen so sehr mag: man kann damit sehr gut Menschen zusammenbringen. Völlig egal wo sie herkommen, wie sie aussehen oder was sie abseits des Spieltischs so tun.  
Wichtig dabei ist natürlich Spiele zu wählen die eben nicht rassistisch oder sexistisch oder sont irgend nen kack sind bzw grade bei älteren Spielen darauf aufmerksam machen um sie nicht unreflektiert zu spielen. Bei neueren sollte auf jedenfall der Verlag informiert werden und hoffentlich gibt es dann Neuauflagen so wie zum Beispiel im Fall von Paleo. In dem Spiel geht es um die Steinzeit aber es wurden ausschließlich hellhäutige Menschen abgebildet was geschichtlich betrachtet einfach nicht stimmen kann. 

Das Logo bzw vorallem auch die ganze Idee dahinter stammt von Udo Bartsch auf seinem Blog [Rezensionen für Millionen](https://rezensionen-fuer-millionen.blogspot.com/2018/10/spielend-fur-toleranz.html)

In der Hinsicht ebenfalls zu empfehlen ist der dritte Teil der HR-Doku über Brettspiele ["Nichts ist Unschuldig"](https://www.ardmediathek.de/video/board-games-willkommen-in-der-welt-der-brettspiele/nichts-ist-unschuldig-s01-e03/hr-fernsehen/Y3JpZDovL2hyLW9ubGluZS8xNTI5NDk/).  
Wobei natürlich die anderen Teile ebenfalls ziemlich cool sind.
