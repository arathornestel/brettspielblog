---
author: "me"
title: "Top3 Weihnachten"
date: 2021-12-25T21:30:26+01:00
#draft: true
description: "Meine drei liebsten Spiele die ich an Weihnachten gespielt habe"
schlagworte: ["toplisten", "gespielt"]
ShowToc: false
---
Heute nur ein kleiner Mini-Rückblick auf die Spiele der Feiertage.

### 1. My Gold Mine
Hat mir sehr gut gefallen obwohl ich in mindestens 3 Runden jedesmal komplett verloren habe einmal sogar ohne einen einziges Gold. Aber es war trotzdem eine Riesen Gaudi und ein gegenseitiges sticheln das man doch ohne Probleme noch ein bisschen Tiefer graben kann.
![Bild von My Gold Mine](/img/mygoldmine-1.jpg)
### 2. Calico
Super schönes Spiel a la "easy to learn and hard to master!". Bin selber halt auch definitiv ein Katzenmensch. Einziges Manko: In der deutschen Version von Ravensburger wurden die Englischen Namen der echten Katzen ins deutsche "übersetzt".
### 3. Fantastische Reiche
Vom Schwierigkeitsgrad ähnlich wie bei Calico. Sehr schönes Artwork und es macht einfach Spaß die vielen verschiedenen Karten zu entdecken und zu kombinieren um sein eigenes Reich zu erschaffen. Hier auf dem Blog gibt es auch eine [Vorstellung](https://brettspielrat.de/fantastische-reiche) davon.