---
author: "me"
title: "Packliste: Weihnachten/Silvester"
date: "2021-12-20"
description: "Spiele die ich für Weihnachten/Silvester mitnehme"
tags: ["Packliste"]
ShowToc: false
#draft: true
---
Die Weihnachtsfeiertage bis Silvester verbringe Ich mit meiner Freundin bei meinen Schwiegereltern, meinen Eltern sowie bei Freunden. Da dürfen natürlich auch ein paar Brettspiele nicht fehlen!  
Welche das sind stelle ich euch im folgenden vor:  
* Azul: Der Sommerpavillion, 2-4 Spielende, 30-45 Minuten, von Michael Kiesling
* Bios: Gensis, 1-4 Spielende, 1-2 Stunden, von Phil Eklund
* Calico, 1-4 Spielende, 30-45 Minuten, von  Kevin Russ
* Canvas, 1-5 Spielende, 30 Minuten, von Jeffrey Chin & Andrew Nerger
* Das Streben nach Glück, 1-4 Spielende, 60-90 Minuten, von David Chircop & Adrian Abela
* Dungeon Twister, 2 Spielende, 45 Minuten, von  Christophe Boelinger
* Fantastische Reiche, 2-6 Spielende, 20 Minuten, von Bruce Glassco
* Gold, 2-5 Spielende, 15 Minuten, von Reiner Knizia

Update 24.12.
### Spiele die ich an Weihnachten vermisst habe
* Double
* Codenames