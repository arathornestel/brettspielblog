# Brettspielblog

## Beschreibung
Dieses Repository enthält die Hugo Dateien für meinen Blog [Thorgrim's Brettspielwelt](thorgrim.uber.space). 
## Roadmap
* [ ] Helles Theme des Blogs mit Farben aus dem nordtheme anpassen
* [ ] Verbindung ins Fediverse etablieren
 
## Contributing
Falls ihr Fehler auf der Seite entdeckt könnt ihr gerne Issues oder Merge Requests aufmachen.
Unter content findet ihr die Einzelseiten des Blogs, unter Layout einige Anpassungen an das PaperMod-Theme und unter static sind CSS-Files sowie Bilder.

## License
For open source projects, say how it is licensed.
